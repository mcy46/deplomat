lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name = "deplomat"
  spec.description = "Stack agnostic deployment system that uses bash and ssh commands".freeze
  spec.summary = "Deplomat".freeze
  spec.homepage = "https://gitlab.com/hodlhodl-public/deplomat"
  spec.license = "MIT"
  spec.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if spec.respond_to? :required_rubygems_version=
  spec.require_paths = ["lib".freeze]
  spec.authors = ["romanitup".freeze]
  spec.extra_rdoc_files = [ "LICENSE.txt", "README.md"]
  spec.version = File.readlines(File.expand_path(File.dirname(__FILE__)) + "/VERSION")[0]
  spec.files = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.require_paths = ["lib"]

  spec.add_dependency "colorize"
  spec.add_dependency "sys-proctable"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rspec"
end
