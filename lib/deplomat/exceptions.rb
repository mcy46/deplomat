module Deplomat
  class ExecutionError  < Exception;end
  class NoSuchPathError < Exception;end
end
