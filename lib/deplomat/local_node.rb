module Deplomat

  class LocalNode < Node

    def initialize(logfile: "#{Dir.pwd}/deplomat.log", log_to: [:stdout], path: Dir.pwd, raise_exceptions: true)
      super
    end

    def path_exist?(path, log_not_found: true)
      exists = File.exist?(adjusted_path(path))
      log "NOT FOUND: #{path}" if log_not_found && !exists
      exists
    end
    alias :path_exists? :path_exist?
    alias :file_exists? :path_exist?

  end

end
