require 'sys/proctable'
require 'open3'
require 'colorize'
require_relative 'deplomat/exceptions'
require_relative 'deplomat/node'
require_relative 'deplomat/local_node'
require_relative 'deplomat/remote_node'
require_relative 'deplomat/directives'
