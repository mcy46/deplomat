require 'rspec/expectations'
require "fileutils"
require_relative "../lib/deplomat"

RSpec.configure do |config|

  config.before(:each) do
    cleanup!
  end

  config.after(:each) do
    cleanup!
  end

  def cleanup!
    FileUtils.rm_r(Dir.glob("#{@fixtures_dir}/dir2/*"), force: true)
    FileUtils.rm_r(Dir.glob("#{@fixtures_dir}/uploaded1/*"), force: true)
    FileUtils.rm_r(Dir.glob("#{@fixtures_dir}/uploaded2/*"), force: true)
    FileUtils.rm_r("#{@fixtures_dir}/existing_file.txt", force: true)
  end

end

RSpec::Matchers.define :have_files do |*expected|
  match do |actual|
    actual = actual.split("\n")
    actual.pop if actual.last == ""
    expected.each do |e|
      return false unless actual.include?(e)
    end
    true
  end
  failure_message do |actual|
    "expected these files in the directory:\n\t#{actual.split("\n").inspect}\nwould include all of these:\n\t#{expected.inspect}"
  end
end

RSpec::Matchers.define :not_have_files do |*expected|
  match do |actual|
    actual = actual.split("\n")
    actual.pop if actual.last == ""
    expected.each do |e|
      return false if actual.include?(e)
    end
    true
  end
  failure_message do |actual|
    "expected these files in the directory:\n\t#{actual.split("\n").inspect}\nwould NOT include any of these:\n\t#{expected.inspect}"
  end
end
