require 'spec_helper'

describe Deplomat::RemoteNode do

  before(:all) do
    # To test remot node, we actually connect to localhost through SSH and pretend it's a remote machine
    @fixtures_dir       = File.expand_path(File.dirname(__FILE__)) + "/remote_node_fixtures"
    @local_fixtures_dir = File.expand_path(File.dirname(__FILE__)) + "/fixtures"
    begin
      @node = Deplomat::RemoteNode.new(host: "localhost", user: ENV['USER'])
    rescue
      raise "RemoteNode tests require your local machine to be able connect to itself through ssh with your current user.\n" +
            "Please add your ~/.ssh/id_rsa.pub to ~/.ssh/authorized_keys and try again."
    end
    @node.log_to = []
  end

  before(:each) do
    @node.raise_exceptions = true
  end

  after(:all) do
    @node.close
  end

  it "updates requisite counter" do
    @node.log_to = []

    requisites_counter_fn = "#{Dir.pwd}/spec/.deployment_requisites_counter"
    @node.cd Dir.pwd + "/spec"

    File.open(requisites_counter_fn, "w") { |f| f.puts 1 }
    @node.update_requisite_number!(2)
    expect(File.readlines(requisites_counter_fn).first.to_i).to eq(2)

    expect(-> { @node.update_requisite_number!(1) }).to raise_exception(SystemExit)

    File.unlink(requisites_counter_fn)
    expect(-> { @node.update_requisite_number!(3) }).to raise_exception(SystemExit)
  end

  describe "filesystem operations" do

  it "checks if file exists" do
    expect(@node.file_exists?("#{@fixtures_dir}/existing_file.txt")).to be_falsey
    expect(@node.file_exists?("/non-existent-path")).to be_falsey
    FileUtils.touch("#{@fixtures_dir}/existing_file.txt")
    puts "#{@fixtures_dir}/existing_file.txt"
    expect(@node.file_exists?("#{@fixtures_dir}/existing_file.txt")).to be_truthy
  end

  it "changes directory correctly adding another slash" do
    @node.current_path = Dir.pwd
    @node.create_dir("tmp")
    @node.cd("tmp/")
    expect(@node.current_path).to eq("#{Dir.pwd}/tmp")
    @node.cd("../")
    expect(@node.current_path).to eq(Dir.pwd)
  end

    it "copies files to another folder" do
      @node.copy("#{@fixtures_dir}/dir1/*", "#{@fixtures_dir}/dir2/")
      expect(@node.execute("ls #{@fixtures_dir}/dir2/")[:out]).to have_files("file1", "file2", "subdir1")
    end

    it "moves files and dirs from one directory into another" do
      @node.move("#{@fixtures_dir}/dir1/*", "#{@fixtures_dir}/dir2/")
      expect(@node.execute("ls #{@fixtures_dir}/dir1/")[:out]).to eq("")
      expect(@node.execute("ls #{@fixtures_dir}/dir2/")[:out]).to have_files("file1", "file2", "subdir1")
      @node.move("#{@fixtures_dir}/dir2/*", "#{@fixtures_dir}/dir1/")
      expect(@node.execute("ls #{@fixtures_dir}/dir2/")[:out]).to eq("")
      expect(@node.execute("ls #{@fixtures_dir}/dir1/")[:out]).to have_files("file1", "file2", "subdir1")
    end

    it "removes files and dirs" do
      @node.copy("#{@fixtures_dir}/dir1/*", "#{@fixtures_dir}/dir2/")
      expect(@node.execute("ls #{@fixtures_dir}/dir2/")[:out]).to have_files("file1", "file2", "subdir1")
      @node.remove("#{@fixtures_dir}/dir2/*")
      expect(@node.execute("ls #{@fixtures_dir}/dir2/")[:out]).to eq("")
    end

    it "uploads files from localhost to the node" do
      @node.upload("#{@local_fixtures_dir}/upload1/*", "#{@fixtures_dir}/uploaded1/", except: 'except1')
      expect(@node.execute("ls #{@fixtures_dir}/uploaded1/")[:out]).to have_files("uploaded_file1", "uploaded_file2")
      expect(@node.execute("ls #{@fixtures_dir}/uploaded1/")[:out]).to not_have_files("except1")
    end

    it "changes current directory" do
      #@node.cd("/etc")
      #expect(@node.current_path).to eq("/etc/") 
      expect(-> { @node.cd("/non-existent-path") }).to raise_exception(Deplomat::NoSuchPathError)
    end

    it "executes commands from the current directory" do
      @node.cd("#{@fixtures_dir}/dir1")
      expect(@node.execute("ls")[:out]).to have_files("file1", "file2", "subdir1")
    end

    it "creates an empty file" do
      @node.cd("#{@fixtures_dir}/dir2")
      @node.touch("myfile")
      expect(@node.execute("ls")[:out]).to have_files("myfile")
    end

    it "creates a directory" do
      @node.cd("#{@fixtures_dir}/dir2")
      @node.mkdir("mydir")
      expect(@node.execute("ls")[:out]).to have_files("mydir")
    end

    it "creates a symlink" do
      @node.cd("#{@fixtures_dir}/dir2")
      @node.create_symlink("../dir1", "./")
      expect(@node.execute("ls dir1")[:out]).to have_files("file1", "file2", "subdir1")
    end

    it "doesn't log ls command when checking if folder exists in `cd`" do
      expect(@node).to receive(:log).with("(deplomat-test-node) --> ls /home/deploy/deplomat/dir2\n", { color: "white"}).exactly(0).times
      @node.cd("#{@fixtures_dir}/dir2")
      allow(@node).to receive(:log)
    end

  end

  describe "handling status and errors" do

    it "returns the status" do
      @node.raise_exceptions = false
      expect(@node.execute("ls #{@fixtures_dir}/dir1/")[:status]).to eq(0)
      expect(@node.execute("ls #{@fixtures_dir}/non-existent-dir/")[:status]).to eq(2)
    end

    it "raises execution error when command fails" do
      @node.raise_exceptions = true
      expect( -> { @node.execute("ls #{@fixtures_dir}/non-existent-dir/")[:status] }).to raise_exception(Deplomat::ExecutionError)
    end

  end

  describe "cleaning" do

    before(:each) do
      @node.cd("#{@fixtures_dir}/cleaning")
      @node.execute("touch file1 file2 file3")
      @node.execute("mkdir current")
      sleep(0.01)
      @node.execute("mkdir dir1 dir2 dir3")
    end

    after(:each) do
      @node.execute("rm -rf #{@fixtures_dir}/cleaning/*")
    end

    it "cleans all the files and dirs in a given directory" do
      @node.clean
      expect(@node.execute('ls')[:out]).to be_empty
    end

    it "cleans all but last 3 entries in a given directory" do
      @node.clean(leave: [3, :last])
      expect(@node.execute('ls')[:out]).to eq("dir1\ndir2\ndir3\n")
    end

    it "cleans all, but :except entries in a given directory" do
      @node.clean(except: ["current"])
      expect(@node.execute('ls')[:out]).to eq("current\n")
    end

  end

end
