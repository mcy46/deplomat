require 'spec_helper'

describe "directives" do

  before(:each) do
    $deplomat_tasks = []
    @fixtures_dir   = File.expand_path(File.dirname(__FILE__)) + "/fixtures"
    @requisites_dir = File.expand_path(File.dirname(__FILE__)) + "/deployment_requisites"
  end

  it "executes arbitrary ruby code in a particular environment" do
    $env = 'staging'
    expect(self).to receive(:hello).exactly(1).times
    execute_in(env: 'staging')    { hello() }
    execute_in(env: 'production') { hello() }
  end

  it "creates a partial" do
    partial("hello_partial") { hello() }
    expect($partials["hello_partial"][:block]).to be_kind_of(Proc)
  end

  it "executes a partial" do
    expect(self).to receive(:hello).with("hello").exactly(1).times
    partial("hello_partial") { |args| hello(args[:x]) }
    execute_partial "hello_partial", x: 'hello'
  end

  it "executes tasks consecutively, running before and after callbacks" do

    $out = ""
    def task1; $out += "task1"; end
    def task2; $out += "task2"; end
    def task3; $out += ",task3"; end

    before_task(:task1, 1) { $out += "before_task1:" }
    after_task(:task1,  2) { $out += ":after_task1,"  }
    before_task(:task2, 3) { $out += "before_task2:" }
    after_task(:task2,  4) { $out += ":after_task2"  }
    before_task(:task4, 5) { $out += "before_task4:" }
    after_task(:task4,  6) { $out += ":after_task4"  }

    add_task :task1, :task2, :task3

    execute_tasks!
    expect($out).to eq("before_task1:task1:after_task1,before_task2:task2:after_task2,task3")

  end

  it "loads and runs requisites" do

    def task1;end
    def task2;end

    11.times do |i|
      File.open("#{@requisites_dir}/#{i+1}_req.rb", "w") do |f|
        f.puts "$deployment_requisites_test ||= []"
        f.puts "$deployment_requisites_test << #{i+1}"
      end
    end

    add_task :task1, :task2

    result = load_requisites!(1, requisites_path: @requisites_dir)
    expect(result[:counter]).to eq(11)
    (2..11).to_a.each do |i|
      expect(result[:log]).to include("Loading requisite: #{i}_req.rb")
    end
    execute_tasks!
    expect($deployment_requisites_test).to eq((2..11).to_a)

    11.times do |i|
      File.unlink("#{@requisites_dir}/#{i+1}_req.rb")
    end

  end

end
